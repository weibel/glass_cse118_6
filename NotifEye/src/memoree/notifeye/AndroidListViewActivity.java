package memoree.notifeye;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import android.app.ListActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
 
public class AndroidListViewActivity extends ListActivity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        //No Title
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //No Notification bar
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
         
        // storing string resources into Array
        String[] notification_list = getResources().getStringArray(R.array.notification_list);
        
        // Binding resources Array to ListAdapter
        this.setListAdapter(new ArrayAdapter<String>(this, R.layout.list_item, R.id.label, notification_list));
        
        //concatenate information to ListView
        notification_list = GlobalVars.addStats(notification_list);
        
        //CALA ADDED THIS HERE
        setListAdapter(new MobileArrayAdapter(this, notification_list));
        
        ListView lv = getListView();
        
        // listening to single list item on click
        lv.setOnItemClickListener(new OnItemClickListener() {
        public void onItemClick(AdapterView<?> parent, View view,
        	int position, long id) {
               
            	   
	            // selected item 
	        	TextView txtview = ((TextView) view.findViewById(R.id.label));
	            //fix
	            String product = txtview.getText().toString();
	        
	            // Launching new Activity on selecting single List Item
	            Intent i = new Intent(getApplicationContext(), SingleListItem.class);
	            // sending data to new activity
	            i.putExtra("product", product);
	            if(!product.contains("Notifications"))
	            	startActivity(i);
   
          	}
        });
    }
}