package memoree.notifeye;

import android.annotation.SuppressLint;
import android.widget.Toast;
import android.content.Context;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


@SuppressLint("NewApi")
public class GlobalVars {
	
	static int drOrdersUCount = 1;
	static int medicationUCount = 0;
	static int labResultsUCount = 2;
	static int bloodBankUCount = 0;
	
	static int drOrdersNewCount = 1;
	static int medicationNewCount = 0;
	static int labResultsNewCount = 2;
	static int bloodBankNewCount = 0;
	
	static String pad = "                                ";
	
	static List<String> sampleList= new ArrayList<String>();
	static List<String> drOrdersList = new ArrayList<String>(Arrays.asList("Need clean syringes in RM405",
	                                          "Send Dr.House to the ER now",
	                                          "URGENT: Nurse Joy to ICU"));
	static List<String> medicationList = new ArrayList<String>(Arrays.asList("Abby Abbikins - Morphine",
	                                            "Bret Bretford - Vicodin",
	                                            "Cole Colson - Antibiotics",
	                                            "John Ho - Chocolate"));
	static List<String> labResultsList = new ArrayList<String>(Arrays.asList("Dr.Jenkins",
												"URGENT: Dr. Sun",
												"URGENT: Dr. Wu",
	                                            "Dr.Lee"));
	static List<String> bloodBankList =new ArrayList<String>( Arrays.asList("Order refill for Type: O-"));
	
	public void GlobalVars()
	{
		
	}

	public String[] getStringArray(String category)
	{
	    List<String> list = new ArrayList<String>();
	    if(category.contains("Dr. Orders")){
            list = drOrdersList;
        }
        else if(category.contains("Medication")){
            list = medicationList;
        }
        else if(category.contains("Lab Results")){
            list = labResultsList;
        }
        else if(category.contains("Blood Bank")){
            list = bloodBankList;
        }
	    return Arrays.copyOf(list.toArray(), list.toArray().length, String[].class);
	}
	public static String getCategory(String category)
	{
			if(category.contains("Dr. Orders")){
	            return "Dr. Orders";
	        }
	        else if(category.contains("Medication")){
	        	return "Medication";
	        }
	        else if(category.contains("Lab Results")){
	        	return "Lab Results";
	        }
	        else if(category.contains("Blood Bank")){
	            return "Blood Bank";
	        }
	        else
	        	return "Notifications";

	}
	public String removeOrder(String category, int index)
	{

	    int ind = index;
	    List<String>_list = new ArrayList();
	    if(category.contains("Dr. Orders")){
	        _list = drOrdersList;
	    }
	    else if(category.contains("Medication")){
	        _list = medicationList;
	    }
	    else if(category.contains("Lab Results")){
	        _list = labResultsList;
	    }
	    else if(category.contains("Blood Bank")){
	        _list = bloodBankList;
	    }
	    
	    if(_list.size() > 0 && index > -1)
	        _list.remove(index);
	    
	    String the_rest = "";
	    for(String s : _list)
	    {
	        the_rest += s +"/n";
	    }
	    
	    return the_rest;
	}
	public static int getTotalNotifications()
	{
	    int total = 0;
	    total = drOrdersList.size() + medicationList.size() + labResultsList.size() + bloodBankList.size();
	    return total;
	}
	
	public static String[] addStats(String[] notification_list){
        List<String> num_list = new ArrayList<String>();
        for(String s: notification_list) {

    		if (s.contains("Dr. Orders") && (drOrdersList.size()>0) ) {
    			num_list.add(s + urgentNotification(drOrdersList) + pad +"         " + paddingListNum(drOrdersList.size()) + drOrdersList.size());
    		} else if (s.contains("Medication") && (medicationList.size()>0)) {
    			num_list.add(s + urgentNotification(medicationList) + pad + "       " + paddingListNum(medicationList.size()) +medicationList.size());
    		} else if (s.contains("Lab Results") && (labResultsList.size()>0)) {
    			num_list.add(s + urgentNotification(labResultsList) + pad + "      " + paddingListNum(labResultsList.size()) + labResultsList.size());
    		} else if (s.contains("Blood Bank") && (bloodBankList.size()>0)) {
    			num_list.add(s + urgentNotification(bloodBankList)+ pad + "        " + paddingListNum(bloodBankList.size()) +bloodBankList.size());
    		} else  if(s.contains("Notifications")) {
                num_list.add(s + "" + paddingListNum(getTotalNotifications()) + getTotalNotifications());
            } else {
                num_list.add(s);
            }
        }
        return num_list.toArray(new String[num_list.size()]);
	}
	
	public static String urgentNotification(List<String> list){
		
		int count = 0;
		for (String s: list){
			if (s.contains("URGENT")){
				count++;
			}
		}
		if (count > 0){
			return " > "+ count + " Urgent" + paddingListNum(count);
		}
		else {
			return "                   ";
		}
		
		
	}
	
	public static String paddingListNum(int size){
		String s ="";
		
			
			if(size > 9){
				s += "   ";
			}
			else if (size >99){
				s += "  ";
			}
			else if (size >999){
				s +=" ";
			}
			
		return s;
	}

}
