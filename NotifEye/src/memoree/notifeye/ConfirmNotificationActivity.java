package memoree.notifeye;


import java.io.Serializable;
import java.util.Arrays;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ListActivity;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.ViewTreeObserver.OnTouchModeChangeListener;
import android.view.Window;
import android.view.ViewTreeObserver;

public class ConfirmNotificationActivity extends Activity{
    
    private static final int KEY_SWIPE_DOWN = 4;//SWIPE DOWN
    private static final int KEYCODE_DPAD_CENTER = 23;//TAP
    View activityRootView;
    
    int _index = -1;
    String _product = "product";
    GlobalVars _gb = new GlobalVars();
    
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.confirm_screen);
        
        activityRootView = findViewById(R.id.anywhere);
        Intent intent = getIntent();
        String product = intent.getStringExtra("product");
        int index = Integer.parseInt(intent.getStringExtra("index"));
        //GlobalVars gb = (GlobalVars)intent.getSerializableExtra("global");
        _product = product;
        _index = index;
        
      //  Toast.makeText(getApplicationContext(), "cancel/confirm " + (int)_index, Toast.LENGTH_SHORT).show();
	}
        	
        
        @Override
        public boolean onKeyUp(int keyCode, KeyEvent event)
        {
                Intent i = new Intent(getApplicationContext(), SingleListItem.class); 
 
                if(keyCode == KEYCODE_DPAD_CENTER)//tap
                {    // there was a swipe down event
                    
                    i.putExtra("confirm", (int)_index);
                    i.putExtra("product", _product);
                    String the_rest =  _gb.removeOrder(_product, _index);
                   // Toast.makeText(getApplicationContext(), "list: " + the_rest, Toast.LENGTH_LONG).show();
                    startActivity(i);
                } 
                  
                else if(keyCode == KEY_SWIPE_DOWN)//swipe down, cancel
                {  
                    i.putExtra("cancel", (int)_index);
                    i.putExtra("product", _product);
                    startActivity(i);
                }
     
            return false;
        }

	}

