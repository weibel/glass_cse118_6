package memoree.notifeye;


import memoree.notifeye.R;
 
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
 
public class MobileArrayAdapter extends ArrayAdapter<String> {
	private final Context context;
	private final String[] values;
 
	public MobileArrayAdapter(Context context, String[] values) {
		super(context, R.layout.list_item, values);
		this.context = context;
		this.values = values;
	}
 
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context
			.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
 
		View rowView = inflater.inflate(R.layout.list_item, parent, false);
		TextView textView = (TextView) rowView.findViewById(R.id.label);
		ImageView imageView = (ImageView) rowView.findViewById(R.id.logo);
		textView.setText(values[position]);
 
		// Change icon based on name
		String s = values[position];
 
		System.out.println(s);
 
		//TODO: MUST ADD NOTIFICATION IF/ELSE

		boolean new_notifications = true;
		if (s.contains("Dr. Orders")) {
			if(GlobalVars.drOrdersList.size() > 0){
				imageView.setImageResource(R.drawable.blue_new);
			}
			else{
				imageView.setImageResource(R.drawable.blue_old);
			}
		} else if (s.contains("Medication")) {
			if(GlobalVars.medicationList.size() > 0 ){
				imageView.setImageResource(R.drawable.white_new);
			}
			else{
				imageView.setImageResource(R.drawable.white_old);
			}
		} else if (s.contains("Lab Results")) {
			if(GlobalVars.labResultsList.size() > 0){
				imageView.setImageResource(R.drawable.green_new);
			}
			else{
				imageView.setImageResource(R.drawable.green_old);
			}
		} else if (s.contains("Blood Bank")) {
			if(GlobalVars.bloodBankList.size() > 0){
				imageView.setImageResource(R.drawable.red_new);
			}
			else{
				imageView.setImageResource(R.drawable.red_old);
			}
		} else {
			imageView.setImageResource(R.drawable.notification_icon);
		}

		return rowView;
	}
}