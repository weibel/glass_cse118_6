package memoree.notifeye;

import java.io.Serializable;
import java.util.Arrays;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
public class SingleListItem extends Activity{
	 ListView listView ;
	 GlobalVars gb = new GlobalVars();//(GlobalVars)i.getSerializableExtra("global");
	 private static final int KEY_SWIPE_DOWN = 4;
	 private String tempProduct = "";
    @SuppressLint("NewApi")
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.single_list_item_view);
         
        TextView txtProduct = (TextView) findViewById(R.id.product_label);
        
        // Get ListView object from xml
        listView = (ListView) findViewById(R.id.list);
        
        Intent i = getIntent();
        
        //if this is from the confirmactivity then we 
        //want to just remove the confirmed notification
        //from the list and restart the activity
        Bundle extras = i.getExtras(); 
        if(i.getStringExtra("confirm") != null || i.getStringExtra("cancel") !=null)
        {
            int index = -1;//extras.getInt("confirm");//
            if(i.getStringExtra("confirm") !=null)
                index = extras.getInt("confirm");
            String product = i.getStringExtra("product");

            Intent _intent = new Intent(getApplicationContext(), SingleListItem.class);
            _intent.putExtra("product", product);
            
            startActivity(i);
        }
        String product = i.getStringExtra("product");
       
        // displaying selected product name4
        product = GlobalVars.getCategory(product);
        txtProduct.setText(product);
        this.setTitle(product);
     

        String[] values = gb.getStringArray(product);
        
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, android.R.id.text1, values);
        
        // Assign adapter to ListView
        listView.setAdapter(adapter); 
        
        // ListView Item Click Listener
        tempProduct = product;
        listView.setOnItemClickListener(new OnItemClickListener() {
        	 //if(product.equals(""))
              @Override
              public void onItemClick(AdapterView<?> parent, View view,
                 int position, long id) {
                
               // ListView Clicked item index
               int itemPosition     = position;
              
               // ListView Clicked item value
               String  itemValue    = (String) listView.getItemAtPosition(position);
              
               Intent i = new Intent(getApplicationContext(), ConfirmNotificationActivity.class); 
               
               i.putExtra("product", tempProduct);
               i.putExtra("index", itemPosition +"");
               
               startActivity(i);
               
              }

         }); 

    }
    
    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event)
    {
        if (keyCode == KEY_SWIPE_DOWN)
        {
            // there was a swipe down event, so go back to the main category menu
            Intent i = new Intent(getApplicationContext(), AndroidListViewActivity.class); 
            startActivity(i);
            return true;
        }
        return false;
    }
}
